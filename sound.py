import pygame

class Sound(pygame.mixer.Sound):
	def __init__(self, path):
		super(Sound, self).__init__(path)

	def play(self, *args, **kwargs):
		super(Sound, self).play(*args, **kwargs)
