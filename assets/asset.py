import pygame

class Asset(object):
	imagePaths = {
		"sprite": None
	}
	soundPaths = {}

	def __init__(self, imageLoader, soundLoader, collisionDetector):
		super(Asset, self).__init__()
		self._images = {}
		self._sounds = {}
		self._imageLoader = imageLoader
		self._soundLoader = soundLoader
		self._collisionDetector = collisionDetector

	@classmethod
	def loadAsset(cls, imageLoader, soundLoader, collisionDetector, saveState):
		asset = cls(imageLoader, soundLoader, collisionDetector)
		asset._loadState(saveState)
		asset.postInit()
		return asset

	@classmethod
	def newAsset(cls, position, imageLoader, soundLoader, collisionDetector):
		asset = cls(imageLoader, soundLoader, collisionDetector)
		asset.initialise(position)
		asset.postInit()
		return asset

	def initialise(self, position):
		self._dead = False
		self.collidable = False
		self._rect = pygame.Rect(position, (0,0))

	def postInit(self):
		self._loadImages()
		self._loadSounds()

		self.mask = pygame.mask.from_surface(self._images['sprite'])

	def _loadSounds(self):
		for name, path in self.soundPaths.iteritems():
			self._sounds[name] = self._soundLoader.loadSound(path)

	def _loadImages(self):
		self._images['sprite'] = self._imageLoader.loadImage(
			self.imagePaths['sprite']
		)

		self.rect.size = (
			self._images['sprite'].get_rect().width,
			self._images['sprite'].get_rect().height,
		)

	def _unloadImages(self):
		for image in self.imagePaths.values():
			self._imageLoader.unloadImage(image)

	def paint(self, display):
		display.blit(self._images['sprite'], self.rect.topleft)

	def update(self, display):
		return True

	@property
	def rect(self):
		return self._rect

	@rect.setter
	def rect(self, rect):
		self._rect = rect

	def clicked(self):
		return True

	def reverseX(self):
		return True

	def reverseY(self):
		return True

	@property
	def dead(self):
		return self._dead

	def getSaveState(self):
		return {
			"__MODULE__": self.__module__,
			"__CLASS__":  self.__class__.__name__,
			"dead": self._dead,
			"rect": tuple(self.rect),
			"imagePaths": self.imagePaths,
			"soundPaths": self.soundPaths,
			"collidable": self.collidable,
		}

	def _loadState(self, state):
		self._dead = state["dead"]
		self.rect = pygame.Rect(state["rect"])
		self.imagePaths = state["imagePaths"]
		self.soundPaths = state["soundPaths"]
		self.collidable = state["collidable"]

