import assets.asset as asset

class Tree(asset.Asset):
	imagePaths = {
		"sprite": ("images", "tree_cartoon_med.png"),
	}

	def __init__(self, imageLoader, soundLoader, collisionDetector):
		"""
			Args:
				position (tuple, position): e.g. (10,30)
				direction (tuple): defines directionX and directionY. Provide them as -1, 0, or 1
				collisionDetector (callable): Callable to detect collisions with any other assets
				imageLoader (ImageLoader): ImageLoader object
				soundLoader (SoundLoader): SoundLoader object

			Returns:
				A new Creep
		"""
		super(Tree, self).__init__(imageLoader, soundLoader, collisionDetector)
