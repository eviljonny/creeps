import assets.asset as asset

class ExplodingMoving(asset.Asset):
	imagePaths = {
		"sprite": ("images", "smile.png"),
		"explosion": ("images", "explosion.png"),
	}

	soundPaths = {
		"explosion": ("sounds", "explosion.ogg"),
	}

	def __init__(self, imageLoader, soundLoader, collisionDetector):
		"""
			Args:
				position (tuple, position): e.g. (10,30)
				direction (tuple): defines directionX and directionY. Provide them as -1, 0, or 1
				collisionDetector (callable): Callable to detect collisions with any other assets
				imageLoader (ImageLoader): ImageLoader object

			Returns:
				A new ExplodingMoving
		"""
		super(ExplodingMoving, self).__init__(imageLoader, soundLoader, collisionDetector)
		self.collidable = True

	@classmethod
	def newAsset(cls, position, imageLoader, soundLoader, collisionDetector, direction=(0,0)):
		asset = cls(imageLoader, soundLoader, collisionDetector)
		asset.initialise(position, direction)
		asset.postInit()
		return asset

	def initialise(self, position, direction=(1,1)):
		self.directionX = direction[0]
		self.directionY = direction[1]

		self.exploding = False
		self.explosionIndex = 0

		super(ExplodingMoving, self).initialise(position)

	def postInit(self):
		# Points is modified by the speed (aka directionX and Y)
		modifier = (0.0 + (abs(self.directionX) + abs(self.directionY))) / 2
		self._pointsValue = 1000 * modifier
		super(ExplodingMoving, self).postInit()

	def _loadImages(self):
		super(ExplodingMoving, self)._loadImages()

		self._images['explosion'] = self._imageLoader.loadSpriteSheet(
			self.imagePaths['explosion'],
			(64, 64),
			22,
		)

	def clicked(self):
		if not self.exploding:
			self.explode()
		return True

	def reverseX(self):
		self.directionX *= -1

	def reverseY(self):
		self.directionY *= -1

	def _advanceExplosion(self):
		self.explosionIndex += 1
		if self.explosionIndex >= len(self._images['explosion']):
			self._unloadImages()
			self._dead = True

	def adjustForEdgeCollision(self, display):
		displayBounds = display.get_rect()
		bounds = self.rect

		(posX, posY) = self.rect.topleft

		if bounds.right >= displayBounds.right \
				or bounds.left <= displayBounds.left:

			if bounds.right >= displayBounds.right:
				self.directionX = abs(self.directionX) * -1
			elif bounds.left <= displayBounds.left:
				self.directionX = abs(self.directionX)

			while self.rect.right >= displayBounds.right \
					or self.rect.left <= displayBounds.left:
				posX += self.directionX
				self.rect.topleft = (posX, posY)

		if bounds.bottom >= displayBounds.bottom \
				or bounds.top <= displayBounds.top:

			if bounds.bottom >= displayBounds.bottom:
				self.directionY = abs(self.directionY) * -1
			elif bounds.top <= displayBounds.top:
				self.directionY = abs(self.directionY)

			while self.rect.bottom >= displayBounds.bottom \
					or self.rect.top <= displayBounds.top:
				posY += self.directionY
				self.rect.topleft = (posX, posY)

	def update(self, display):
		if not self.dead:
			if self.exploding:
				self._advanceExplosion()
			else:
				self.adjustForEdgeCollision(display)

				(posX, posY) = self.rect.topleft

				posX += self.directionX
				self.rect.topleft = (posX, posY)

				collision = self._collisionDetector(self)
				if collision is not None:
					posX -= self.directionX
					self.reverseX()
					collision.reverseX()
					posX += self.directionX

				posY += self.directionY
				self.rect.topleft = (posX, posY)

				collision = self._collisionDetector(self)
				if collision is not None:
					posY -= self.directionY
					self.reverseY()
					collision.reverseY()
					posY += self.directionY

				self.rect.topleft = (posX, posY)


	def paint(self, display):
		if not self.dead:
			if self.explosionIndex <= 11:
				display.blit(self._images['sprite'], self.rect.topleft)

			if self.exploding:
				explosionPosition = (
					self.rect.x - 12,
					self.rect.y - 12
				)
				display.blit(
					self._images['explosion'][self.explosionIndex],
					explosionPosition
				)

	def explode(self):
		self.exploding = True
		self._sounds['explosion'].play()

	@property
	def points(self):
		return self._pointsValue

	def getSaveState(self):
		saveState = super(ExplodingMoving, self).getSaveState()

		saveState.update({
			"exploding": self.exploding,
			"explosionIndex": self.explosionIndex,
			"directionX": self.directionX,
			"directionY": self.directionY,
		})

		return saveState

	def _loadState(self, state):
		super(ExplodingMoving, self)._loadState(state)
		self.exploding = state["exploding"]
		self.explosionIndex = state["explosionIndex"]
		self.directionX = state["directionX"]
		self.directionY = state["directionY"]
