import levels.level as level
import assets.scenery as scenery
import score
import crosshair
import assets.exploding as exploding
import random

class Level1(level.Level):
	soundPaths = {
		"background": ("sounds", "JauntyGumption.ogg"),
		"fanfare": ("sounds", "fanfare.ogg"),
	}
	imagePaths = {
		"background": ("images", "background.png"),
	}

	def __init__(self, imageLoader, soundLoader, screen, collisionDetector, saveState=None):
		random.seed()

		self._maxCreeps = 20
		self._currentCreeps = 0
		self._startCreeps = 10

		super(Level1, self).__init__(
			imageLoader,
			soundLoader,
			screen,
			crosshair.Crosshair.newCrosshair(imageLoader),
			score.Score.newScore((20,20)),
			collisionDetector,
			saveState=saveState
		)

		self._sounds['background'].play(loops=-1)

	def _loadAssets(self):
		super(Level1, self)._loadAssets()

		self.addScenery(scenery.Tree, (680,460))
		self.addScenery(scenery.Tree, (580,460))
		self.addScenery(scenery.Tree, (630,460))
		self.addScenery(scenery.Tree, (280,460))

		for i in xrange(0, self._startCreeps):
			self.addRandomCreep()

	def complete(self):
		super(Level1, self).complete()

		self._sounds['fanfare'].play()
		self._sounds['background'].stop()

		print "Congratulations, you completed the game with a score of %r" % self._score.score

	def addCreep(self, position, direction):
		self._currentCreeps += 1

		self._assets.append(
			exploding.ExplodingMoving.newAsset(
				position,
				self._imageLoader,
				self._soundLoader,
				self._collisionDetector,
				direction=direction,
			)
		)

	def addRandomCreep(self):
		x = random.randint(0, self._screen.get_rect().width - 16)
		y = random.randint(0, self._screen.get_rect().height - 16)
		dx = 0
		dy = 0
		while dx == 0 and dy == 0:
			dx = random.randint(-4, 4)
			dy = random.randint(-4, 4)

		self.addCreep((x,y), (dx, dy))

	def addScenery(self, sceneryObject, position):
		self._assets.append(
			sceneryObject.newAsset(position, self._imageLoader, self._soundLoader, self._collisionDetector)
		)

	def isComplete(self):
		if self._currentCreeps == 0:
			return True

	def _assetReaped(self):
		super(Level1, self)._assetReaped()
		self._currentCreeps -= 1

	def getSaveState(self):
		saveState = super(Level1, self).getSaveState()
		saveState.update({
			"maxCreeps": self._maxCreeps,
			"currentCreeps": self._currentCreeps,
			"startCreeps": self._startCreeps,
		})
		return saveState

	def _loadState(self, state):
		self._maxCreeps = state["maxCreeps"]
		self._currentCreeps = state["currentCreeps"]
		self._startCreeps = state["startCreeps"]
		super(Level1, self)._loadState(state)
