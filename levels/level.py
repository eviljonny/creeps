import importlib

class Level(object):
	soundPaths = {}
	imagePaths = {}

	def __init__(self, imageLoader, soundLoader, screen,
			cursor, score, collisionDetector, saveState=None):

		super(Level, self).__init__()

		self._assets = []
		self._sounds = {}
		self._screen = screen
		self._timestamp = None
		self._collisionDetector = collisionDetector

		self._imageLoader = imageLoader
		self._soundLoader = soundLoader

		if saveState is None:
			self._interval = 5
			self._cursor = cursor
			self._score = score
			self._loadAssets()
		else:
			self._loadState(saveState)

		self._loadSounds()
		self._loadImages()

	def addAsset(self, asset):
		self._assets.append(asset)

	def insertAsset(self, position, asset):
		self._assets.insert(position, asset)

	def _loadAssets(self):
		pass

	def _loadImages(self):
		self._background = self._imageLoader.loadImage(
			self.imagePaths["background"]
		)

	def _loadSounds(self):
		for name, path in self.soundPaths.iteritems():
			self._sounds[name] = self._soundLoader.loadSound(path)

	def update(self, time):
		if self._timestamp is None:
			self._timestamp = time

		if time > self._timestamp + self._interval:
			self._timestamp = time
			if self._currentCreeps < self._maxCreeps:
				self.addRandomCreep()

		self.reapAssets()
		self.updateAssets()

	def updateAssets(self):
		for asset in self._assets:
			asset.update(self._screen)
		self._cursor.update()

	def paint(self):
		self._paintBackground()
		self._paintAssets()
		self._paintScore()
		self._paintCursor()

	def _paintBackground(self):
		self._screen.blit(self._background, (0,0))

	def _paintAssets(self):
		for asset in reversed(self._assets):
			asset.paint(self._screen)

	def _paintScore(self):
		self._score.paint(self._screen)

	def _paintCursor(self):
		self._cursor.paint(self._screen)

	def reapAssets(self):
		deadAssets = []

		for index, asset in enumerate(self._assets):
			if asset.dead:
				self._score.update(asset.points)
				deadAssets.append(index)

		# do the list in reverse order so all indexes dont
		# shift as we delete the dead assets
		for index in reversed(deadAssets):
			self._assetReaped()
			del self._assets[index]

	def _assetReaped(self):
		pass

	def complete(self):
		self._paintBackground()
		self._paintAssets()
		self._paintCursor()
		self._score.forcePaintFinalScore(self._screen)

	def isComplete(self):
		return False

	@property
	def assets(self):
		return self._assets

	def getAssetClicked(self, position):
		for asset in self._assets:
			if asset.rect.collidepoint(position):
				if asset.mask.get_at((
					position[0] - asset.rect.x,
					position[1] - asset.rect.y
				)):
					return asset

		return None

	def getSaveState(self):
		return {
			"__MODULE__": self.__module__,
			"__CLASS__":  self.__class__.__name__,
			"interval": self._interval,
			"assets": [asset.getSaveState() for asset in self._assets],
			"imagePaths": self.imagePaths,
			"soundPaths": self.soundPaths,
			"cursor": self._cursor.getSaveState(),
			"score": self._score.getSaveState(),
		}

	def _loadClass(self, module, className, *args, **kwargs):
		lib = importlib.import_module(module)
		class_ = getattr(lib, className)
		return class_.loadAsset(*args, **kwargs)

	def _loadState(self, state):
		self._interval = state["interval"]
		for asset in state["assets"]:
			self.addAsset(
				self._loadClass(
					asset["__MODULE__"],
					asset["__CLASS__"],
					self._imageLoader,
					self._soundLoader,
					self._collisionDetector,
					saveState=asset
				)
			)

		self.imagePaths = state["imagePaths"]
		self.soundPaths = state["soundPaths"]

		import crosshair
		self._cursor = crosshair.Crosshair.loadCrosshair(self._imageLoader, state["cursor"])
#		self._cursor = self._loadClass(
#			state["cursor"]["__MODULE__"],
#			state["cursor"]["__CLASS__"],
#			self._imageLoader,
#			saveState=state["cursor"]
#		)

		# load Score
		import score
		self._score = score.Score.loadScore(state["score"])
#		self._score = self._loadClass(
#			state["score"]["__MODULE__"],
#			state["score"]["__CLASS__"],
#			(0,0),
#			saveState=state["score"]
#		)
#

