import pygame

class Crosshair(object):
	imagePaths = {
		"crosshair": ("images", "crosshair.png"),
	}

	def __init__(self, imageLoader):
		super(Crosshair, self).__init__()
		self.__images = {}
		self.__imageLoader = imageLoader

	@classmethod
	def loadCrosshair(cls, imageLoader, saveState):
		score = cls(imageLoader)
		score._loadState(saveState)
		score.postInit()
		return score

	@classmethod
	def newCrosshair(cls, imageLoader, position=(50,50)):
		score = cls(imageLoader)
		score.initialise(position)
		score.postInit()
		return score

	def initialise(self, position):
		self.__rect = pygame.Rect(position, (0,0))

	def postInit(self):
		self.__loadImages()

	def __loadImages(self):
		self.__images['crosshair'] = self.__imageLoader.loadImage(
			self.imagePaths['crosshair']
		)

		self.rect.size = (
			self.__images['crosshair'].get_rect().width,
			self.__images['crosshair'].get_rect().height,
		)

	def __unloadImages(self):
		for image in self.imagePaths.values():
			self.__imageLoader.unloadImage(image)

	def update(self):
		self.rect.center = pygame.mouse.get_pos()

	def paint(self, display):
		display.blit(self.__images['crosshair'], self.rect.topleft)

	@property
	def rect(self):
		return self.__rect

	def getSaveState(self):
		return {
			"__MODULE__": self.__module__,
			"__CLASS__":  self.__class__.__name__,
			"rect": tuple(self.rect),
			"imagePaths": self.imagePaths
		}

	def _loadState(self, state):
		self.__rect = pygame.Rect(state["rect"])
		self.imagePaths = state["imagePaths"]
