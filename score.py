import pygame

class Score(object):
	fontFace = 'freesansbold.ttf'
	fontSize = 32

	@classmethod
	def loadScore(cls, saveState):
		score = cls()
		score._loadState(saveState)
		score.postInit()
		return score

	@classmethod
	def newScore(cls, position):
		score = cls()
		score.initialise(position)
		score.postInit()
		return score

	def initialise(self, position):
		self.__score = 0
		self.__displayScore = 0
		self.__position = position

	def postInit(self):
		self.__font = pygame.font.Font(self.fontFace, self.fontSize)

	def update(self, points):
		self.__score += int(points)

	def __renderScore(self, score, display):
		surface = self.__font.render(
			"Score %05d" % score,
			True, # Anti alias
			(255, 0, 0)
		)
		display.blit(surface, self.__position)

	def forcePaintFinalScore(self, display):
		self.__renderScore(self.__score, display)

	def paint(self, display):
		if self.__displayScore < self.__score:
			self.__displayScore += 100

		self.__renderScore(self.__displayScore, display)

	@property
	def score(self):
		return self.__score

	def getSaveState(self):
		return {
			"__MODULE__": self.__module__,
			"__CLASS__":  self.__class__.__name__,
			"score": self.score,
			"displayScore": self.__displayScore,
			"position": self.__position,
			"fontFace": self.fontFace,
			"fontSize": self.fontSize,
		}

	def _loadState(self, state):
		self.__score = state["score"]
		self.__displayScore = state["displayScore"]
		self.__position = state["position"]
		self.fontFace = state["fontFace"]
		self.fontSize = state["fontSize"]
