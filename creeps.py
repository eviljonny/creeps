#!/usr/bin/python

import pygame
import sys
import imageLoader
import soundLoader
import time
import levels.level1 as level1

class Creeps(object):
	def __init__(self):
		super(Creeps, self).__init__()
		self.__fpsLimit = 30
		self.__imageLoader = imageLoader.ImageLoader()
		self.__soundLoader = soundLoader.SoundLoader()

		pygame.init()
		pygame.mixer.init()

		self.displaySize = (800, 600)
		self.__screen = pygame.display.set_mode(self.displaySize)
		pygame.display.set_caption("Creeps example")
		pygame.mouse.set_visible(0)

		self.__level = level1.Level1(
			self.__imageLoader,
			self.__soundLoader,
			self.__screen,
			self.checkForCollision
		)

		self.__fpsClock = pygame.time.Clock()
		self.__interval = 5

	def handleKeyPress(self, event):
		if event.key == pygame.K_q:
			self.quit()
		elif event.key == pygame.K_e:
			for asset in self.__level.assets:
				asset.clicked()
		elif event.key == pygame.K_s:
			self.save()
		elif event.key == pygame.K_l:
			self.load()

	def handleClick(self, event):
		# left click
		if event.button == 1:
			clickedAsset = self.__level.getAssetClicked(event.pos)
			if clickedAsset is not None:
				clickedAsset.clicked()

	def checkForCollision(self, checkAsset):
		if checkAsset.collidable:
			for asset in self.__level.assets:
				if asset.collidable:
					if checkAsset is asset:
						continue

					if pygame.sprite.collide_mask(checkAsset, asset):
						return asset
		return None

	def complete(self):
		self.__level.complete()

		pygame.display.flip()
		self.running = False
		time.sleep(7)

	def run(self):
		self.running = True

		while self.running:
			self.__level.update(time.time())
			self.__level.paint()

			pygame.display.flip()

			if self.__level.isComplete():
				self.complete()

			for event in pygame.event.get():
				if event.type == pygame.QUIT:
					self.quit()
				elif event.type == pygame.KEYDOWN:
					self.handleKeyPress(event)
				elif event.type == pygame.MOUSEBUTTONDOWN:
					self.handleClick(event)

			self.__fpsClock.tick(self.__fpsLimit)

		self.__shutdown()

	def quit(self):
		self.running = False
		print "Good bye..."

	def __shutdown(self):
		pygame.quit()
		sys.exit()

	@property
	def running(self):
		return self.__running

	@running.setter
	def running(self, running):
		self.__running = bool(running)

	def save(self):
		from pprint import pprint

		import pickle
		with open("saveFile.sav", "wb") as outfile:
			pickle.dump(self.__level.getSaveState(), outfile, -1)

		pprint("%s" % self.__level.getSaveState())

	def load(self):
		import pickle
		with open("saveFile.sav", "rb") as saveFile:
			saveState = pickle.load(saveFile)

		self.__level = level1.Level1(
			self.__imageLoader,
			self.__soundLoader,
			self.__screen,
			self.checkForCollision,
			saveState=saveState
		)


if __name__ == "__main__":
	creeps = Creeps()
	creeps.run()
