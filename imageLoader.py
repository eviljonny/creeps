import pygame
import os

class ImageLoader(object):
	def __init__(self):
		super(ImageLoader, self).__init__()

		self.__images = {}

	def applyColorkey(self, images, colorkey=None):
		if colorkey is not None:
			for image in images:
				image.set_colorkey(colorkey)

	def loadImage(self, pathParts, colorkey=None):
		path = os.path.join(*pathParts)

		if path in self.__images:
			self.__images[path]["refcount"] += 1
		else:
			image = pygame.image.load(path)
			image.convert_alpha()
			self.__images[path] = {
				"refcount": 1,
				"image": image
			}

		return self.__images[path]["image"]

	def loadSpriteSheet(self, pathParts, frameSize, frameCount, colorkey=None):
		""" Load a sprite sheet and cut into chunks

			Args:
				pathParts (strings): Path parts for os.path.join(...)
				imageSize (tuple of ints): width and height of the sprite
				frameCount (int): Number of frames in the animation

			Returns:
				tuple of pygame images
		"""
		path = os.path.join(*pathParts)

		if path in self.__images:
			self.__images[path]["refcount"] += 1
		else:

			sheet = pygame.image.load(path)
			(_, _, sheetWidth, sheetHeight) = sheet.get_rect()

			frames = []
			xPos = 0
			yPos = 0

			for index in xrange(frameCount):
				image = pygame.Surface(frameSize, flags=pygame.SRCALPHA)

				currentFrame = pygame.Rect(
					xPos * frameSize[0],
					yPos * frameSize[1],
					frameSize[0],
					frameSize[1],
				)

				if currentFrame.x + currentFrame.width > sheetWidth:
					xPos = 0
					yPos += 1
					currentFrame = pygame.Rect(
						xPos * frameSize[0],
						yPos * frameSize[1],
						frameSize[0],
						frameSize[1],
					)

				image.blit(sheet, (0,0), currentFrame)

				frames.append(image)

				xPos += 1


			self.__images[path] = {
				"refcount": 1,
				"image": frames,
			}

		return self.__images[path]["image"]

	def unloadImage(self, pathParts):
		path = os.path.join(*pathParts)

		if path not in self.__images:
			raise ValueError("Image not found to unload: %r" % path)

		self.__images[path]["refcount"] -= 1

		if self.__images[path]["refcount"] == 0:
			del self.__images[path]

